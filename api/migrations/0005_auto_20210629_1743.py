# Generated by Django 3.2.3 on 2021-06-29 12:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20210527_2218'),
    ]

    operations = [
        migrations.RenameField(
            model_name='record',
            old_name='end',
            new_name='end_recording',
        ),
        migrations.RenameField(
            model_name='record',
            old_name='amount',
            new_name='session_cost',
        ),
        migrations.RenameField(
            model_name='record',
            old_name='start',
            new_name='start_recording',
        ),
    ]
