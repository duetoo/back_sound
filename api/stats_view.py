from datetime import datetime, date
from django.db.models import Sum, Avg, Max
from rest_framework import (views,
                            response,
                            permissions,)
from .models import Record


class StudioStatView(views.APIView):
    def get(self, request, pk):
        queryset = Record.objects.filter(studio_id=pk,
                                         studio__company__owner=self.request.user, 
                                         start_recording__range=
                                         [request.query_params['start_rec'],
                                          request.query_params['end_rec']])
        cost_stat = queryset.aggregate(average_cost=Avg('cost'),
                                       max_cost=Max('cost'),
                                       max_duration=Max('duration'),
                                       total_cost=Sum('session_cost'))
        return response.Response(cost_stat)

    def get_view_name(self):
        return f'Статистика по студии'


class CompanyStatView(views.APIView):
    def get(self, request, pk):
        queryset = Record.objects.filter(studio__company_id=pk,
                                         studio__company__owner=self.request.user)
        amount_stat = queryset.aggregate(total_amount=Sum('amount'),
                                         max_cost=Max('cost'),
                                         avg_cost=Avg('cost'))
        print(amount_stat)
        return response.Response(amount_stat)

    def get_view_name(self):
        return f'Статистика компании'


class EachDayStatView(views.APIView):

    permission_classes = (permissions.AllowAny,)

    def get(self, request, pk):
        my = date(2021, 7, 5)
        queryset = Record.objects.filter(start_recording__gt=my).\
            extra(select={'day': 'date(start_recording)'}).values('day').\
            annotate(amount=Sum('session_cost'))
        return response.Response({'days_stats': queryset})
