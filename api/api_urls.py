from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import (CompanyView,
                    StudioView,
                    RecordsCompanyView,
                    RecordStudioView,
                    UserSignUPView,)
from .stats_view import (StudioStatView,
                         CompanyStatView,
                         EachDayStatView,
                         )

router = DefaultRouter()
router.register('company', CompanyView, basename='companies')
router.register('studios', StudioView, basename='studios')
router.register('records', RecordStudioView, basename='records')

urlpatterns = [
    path('sign_up/', UserSignUPView.as_view()),
    path('', include(router.urls)),
    path('company/records/', RecordsCompanyView.as_view()),
    #path('studio/<int:pk>/records/', ),
    path('company/<int:pk>/stat/', CompanyStatView.as_view()),
    path('studio/<int:pk>/stat/', StudioStatView.as_view()),
    path('<int:pk>/day/stat/', EachDayStatView.as_view()),
]
